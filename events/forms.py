from django import forms
from .models import Event


class EventCreationForm(forms.ModelForm):

    class Meta(forms.ModelForm):
        model = Event
        fields = ("title", "start_date", "start_time", "description")
        widgets = {
            'start_date': forms.DateInput(format=('%Y-%m-%d'),
                                             attrs={'class': 'form-control', 'placeholder': 'Select a date',
                                                    'type': 'date'}),
            'start_time': forms.TimeInput(format='%H:%M',
                                          attrs={'class': 'form-control', 'placeholder': 'Select a timetable',
                                                 'type': "time"}),
        }


class EventUpdateForm(forms.ModelForm):

    class Meta(forms.ModelForm):
        model = Event
        fields = ("title", "start_date", "start_time", "description")
        widgets = {
            'start_date': forms.DateInput(format=('%Y-%m-%d'),
                                          attrs={'class': 'form-control', 'placeholder': 'Select a date',
                                                 'type': 'date'}),
            'start_time': forms.TimeInput(format='%H:%M',
                                          attrs={'class': 'form-control', 'placeholder': 'Select a timetable',
                                                 'type': "time"}),
        }