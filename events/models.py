from django.db import models
from forum.models import Topic
from tinymce.models import HTMLField


class Event(models.Model):
    title = models.CharField(max_length=255, blank=False, null=False)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    start_date = models.DateField(blank=False, null=False)
    start_time = models.TimeField(blank=True, null=True)
    description = HTMLField(blank=True, null=True)

    def __str__(self):
        return f"{self.topic} - {self.title}"
