from django.contrib import admin
from .models import Event


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ("title", "topic", "start_date", "start_time")
    list_filter = ("topic",)
    search_fields = ("title__icontains",)
