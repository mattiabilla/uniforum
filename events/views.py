from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView

from forum.models import Topic
from .forms import EventCreationForm, EventUpdateForm
from .models import Event


class ListEventsView(ListView):
    template_name = "events/events.html"
    model = Event

    def get_context_data(self, **kwargs):
        context = super(ListEventsView, self).get_context_data(**kwargs)
        context['topic'] = get_object_or_404(Topic, pk=self.kwargs['pk'])
        return context

    def get_queryset(self):
        objects = self.model.objects.all()
        objects = objects.filter(topic=self.kwargs['pk'])
        return objects


class DetailEventView(DetailView):
    template_name = "events/view_event.html"
    model = Event


class EventCreateView(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    permission_required = "events.add_event"
    form_class = EventCreationForm
    template_name = "events/add_event.html"
    model = Event
    success_message = "Event created successfully!"

    def get_success_url(self):
        pk = self.kwargs['pk']
        return reverse_lazy('events:events', kwargs={'pk': pk})

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.topic = get_object_or_404(Topic, pk=self.kwargs['pk'])
        return super().form_valid(form)


class EventDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = "events.delete_event"
    model = Event
    template_name = 'events/delete_event.html'
    success_message = "Event deleted successfully!"

    def get_success_url(self):
        return reverse_lazy('events:events', kwargs={"pk": self.get_object().topic.pk})

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(EventDeleteView, self).delete(request, *args, **kwargs)


class EventUpdateView(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    permission_required = "events.change_event"
    model = Event
    template_name = 'events/update_event.html'
    form_class = EventUpdateForm
    success_message = "Event updated successfully!"

    def get_success_url(self):
        return reverse_lazy('events:events', kwargs={"pk": self.get_object().topic.pk})
