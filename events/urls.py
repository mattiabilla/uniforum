from django.urls import path
from .views import ListEventsView, DetailEventView, EventCreateView, EventDeleteView, EventUpdateView

app_name = 'events'

urlpatterns = [
    path('topic/<int:pk>', ListEventsView.as_view(), name='events'),
    path('<int:pk>', DetailEventView.as_view(), name='view-event'),
    path('<int:pk>/add-event', EventCreateView.as_view(), name="add-event"),
    path('<int:pk>/delete-event', EventDeleteView.as_view(), name="delete-event"),
    path('<int:pk>/update-event', EventUpdateView.as_view(), name="update-event"),
]
