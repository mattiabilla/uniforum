from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy, reverse
from django.views import View
from django.views.generic import CreateView, ListView, TemplateView, UpdateView, DeleteView, DetailView

from forum.models import Topic
from .forms import ExamVoteCreationForm, UniUserCreationForm, UniUserChangeForm, ExamVoteUpdateForm
from .models import UniUser, UserExamVote
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType


class UserCreationView(SuccessMessageMixin, CreateView):

    form_class = UniUserCreationForm
    template_name = 'users/creation.html'
    success_url = reverse_lazy('login')
    success_message = "User created successfully!"


class ProfileView(DetailView):
    template_name = "users/profile.html"
    model = UniUser

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        if self.request.user == self.get_object() or not self.get_object().private:
            context['marks'] = UserExamVote.objects.all().filter(user=self.get_object())
        return context


class UserUpdateView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = UniUser
    template_name = 'users/update.html'
    form_class = UniUserChangeForm
    success_message = "User updated successfully!"

    def get_success_url(self):
        pk = self.kwargs['pk']
        return reverse_lazy('users:profile', kwargs={'pk': pk})

    def get_object(self, *args, **kwargs):
        obj = super(UserUpdateView, self).get_object(*args, **kwargs)
        if not obj.id == self.request.user.id:
            raise PermissionDenied
        return obj


class UserListView(LoginRequiredMixin, ListView):
    model = UniUser
    template_name = 'users/list.html'
    paginate_by = 2

    def get_queryset(self):
        username = self.request.GET.get("username")

        object_list = self.model.objects.all()
        if username:
            object_list = object_list.filter(username__icontains=username).order_by('username')
        return object_list

    def get_context_data(self, **kwargs):
        context = super(UserListView, self).get_context_data(**kwargs)
        context['search'] = ""
        if self.request.GET.get("username") is not None:
            context['search'] = self.request.GET.get("username")
        return context


class VoteView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        user_voted = get_object_or_404(UniUser, id=self.kwargs['pk'])
        if user_voted.votes.filter(id=request.user.id).exists():
            user_voted.votes.remove(request.user)
        else:
            user_voted.votes.add(request.user)
        self.check_mod(user_voted)
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    def check_mod(self, user_voted):
        n_users = UniUser.objects.count()
        min_votes = max(int(15*n_users/100), 2)

        mods, created = Group.objects.get_or_create(name="mods")
        if created:
            mods.name = "mods"
            content_type_topic = ContentType.objects.get(app_label='forum', model='topic')
            content_type_post = ContentType.objects.get(app_label='forum', model='post')
            content_type_comment = ContentType.objects.get(app_label='forum', model='comment')
            content_type_events = ContentType.objects.get(app_label='events', model='event')
            content_type_reviews = ContentType.objects.get(app_label='reviews', model='review')
            # get all permssions for these models
            perms = [Permission.objects.filter(content_type=content_type_topic),
                     Permission.objects.filter(content_type=content_type_post),
                     Permission.objects.filter(content_type=content_type_comment),
                     Permission.objects.filter(content_type=content_type_events),
                     Permission.objects.filter(content_type=content_type_reviews)]

            for e in perms:
                for p in e:
                    mods.permissions.add(p)
            mods.save()

        if user_voted.votes.count() >= min_votes:
            user_voted.groups.add(mods)
        else:
            user_voted.groups.remove(mods)


class ExamVoteCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    form_class = ExamVoteCreationForm
    template_name = "users/add_mark.html"
    model = UserExamVote
    success_message = "Mark added successfully!"

    def get_success_url(self):
        pk = self.request.user.pk
        return reverse_lazy('users:profile', kwargs={'pk': pk})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'request': self.request})
        return kwargs

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class ExamVoteUpdateView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = UserExamVote
    template_name = 'users/update_mark.html'
    form_class = ExamVoteUpdateForm
    success_message = "Mark updated successfully!"

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not request.user == self.object.user:
            return HttpResponseForbidden()
        return super().post(request, *args, **kwargs)

    def get_success_url(self):
        pk = self.request.user.pk
        return reverse_lazy('users:profile', kwargs={'pk': pk})


class ExamVoteDeleteView(LoginRequiredMixin, DeleteView):
    model = UserExamVote
    template_name = 'users/delete_mark.html'
    success_message = "Mark deleted successfully!"

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not request.user == self.object.user:
            return HttpResponseForbidden()
        return super().post(request, *args, **kwargs)

    def get_success_url(self):
        pk = self.request.user.pk
        return reverse_lazy('users:profile', kwargs={'pk': pk})

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(ExamVoteDeleteView, self).delete(request, *args, **kwargs)
