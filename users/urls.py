from django.urls import path
from .views import UserCreationView, UserListView, UserUpdateView, VoteView, ExamVoteCreateView, \
    ExamVoteUpdateView, ExamVoteDeleteView, ProfileView

app_name = 'users'

urlpatterns = [
    path('signup/', UserCreationView.as_view(), name="signup"),
    path('<int:pk>/', ProfileView.as_view(), name="profile"),
    path('<int:pk>/update/', UserUpdateView.as_view(), name="update"),
    path('', UserListView.as_view(), name="list"),
    path('vote/<int:pk>', VoteView.as_view(), name="vote-user"),
    path('add-mark', ExamVoteCreateView.as_view(), name="add-mark"),
    path('mark/<int:pk>/update', ExamVoteUpdateView.as_view(), name="update-mark"),
    path('mark/<int:pk>/delete', ExamVoteDeleteView.as_view(), name="delete-mark"),
]
