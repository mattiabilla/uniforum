from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _


class UniUser(AbstractUser):
    email = models.EmailField(_('email address'), blank=False, unique=True)
    propic = models.ImageField(upload_to='images/', null=True, blank=True)
    votes = models.ManyToManyField("self", symmetrical=False, blank=True)
    private = models.BooleanField(blank=False, null=False)


from forum.models import Topic


class UserExamVote(models.Model):
    exam = models.ForeignKey(Topic, blank=False, null=False, on_delete=models.CASCADE)
    user = models.ForeignKey(UniUser, blank=False, null=False, on_delete=models.CASCADE)
    vote = models.IntegerField(validators=[MaxValueValidator(30), MinValueValidator(18)], blank=False, null=False)
    honors = models.BooleanField(blank=False, null=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["exam", "user"],
                name="A user can have just one vote per exam",
            )
        ]

    def clean(self, *args, **kwargs):
        super().clean(*args, **kwargs)
        if self.vote < 30 and self.honors:
            raise ValidationError("Honors could be assigned only if vote is equal to 30")
        if not self.exam.is_exam:
            raise ValidationError("Topic must be an exam")

    def __str__(self):
        return f"{self.user} - {self.exam}"
