from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _

from forum.models import Topic
from .models import UniUser, UserExamVote


class UniUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = UniUser
        fields = ("username", "email", "first_name", "last_name", "propic", "private")
        success_url = reverse_lazy('homepage')
        labels = {
            'propic': _('Profile Picture'),
        }


class UniUserChangeForm(UserChangeForm):
    password = None

    class Meta(UserChangeForm):
        model = UniUser
        fields = ("username", "email", "first_name", "last_name", "propic", "private")
        labels = {
            'propic': _('Profile Picture'),
        }


class ExamVoteCreationForm(forms.ModelForm):
    class Meta:
        model = UserExamVote
        fields = ('exam', 'vote', 'honors', )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(ExamVoteCreationForm, self).__init__(*args, **kwargs)
        exams = Topic.objects.filter(is_exam=True)
        exams_user = UserExamVote.objects.filter(user=self.request.user).values('exam')
        exams_user = [el["exam"] for el in exams_user]
        self.fields['exam'].queryset = exams.exclude(id__in=exams_user)


class ExamVoteUpdateForm(forms.ModelForm):
    class Meta:
        model = UserExamVote
        fields = ('vote', 'honors', )

