from django.contrib import auth
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db import IntegrityError
from django.http import request
from django.shortcuts import get_object_or_404
from django.test import TestCase
from django.urls import reverse_lazy, reverse
from django.contrib.auth.models import Permission

from forum.models import Topic
from .models import UniUser, UserExamVote


class UserTestCase(TestCase):
    def setUp(self):
        UniUser.objects.create_user(email="prova1@email.com", username="prova1",
                                    first_name="prova", last_name="va",
                                    password="test_password_1", private=False)
        UniUser.objects.create_user(email="prova2@email.com", username="prova2",
                                    password="test_password_2", private=True)
        UniUser.objects.create_user(email="prova3@email.com", username="prova3",
                                    first_name="provaaaa",
                                    password="test_password_3", private=True)
        UniUser.objects.create_user(email="prova4@email.com", username="prova4",
                                    last_name="vava4",
                                    password="test_password_4", private=False)
        UniUser.objects.create_user(email="prova5@email.com", username="prova5",
                                    password="test_password_5", private=False)

    def test_setup_users(self):
        self.assertEqual(UniUser.objects.all().count(), 5)

    def test_create_existing_user(self):
        with self.assertRaises(IntegrityError) as e:
            UniUser.objects.create_user(email="prova1@email.com", username="prova1",
                                        first_name="prova", last_name="va",
                                        password="test_password_1", private=False)
        self.assertEqual(IntegrityError, type(e.exception))

    def test_create_user(self):
        response = self.client.post(reverse('users:signup'), {
            'email': 'prova6@email.com',
            'username': 'prova6',
            'first_name': 'pp6',
            'private': False
        })

        self.assertEqual(response.status_code, 200)

    def test_user_login(self):
        response = self.client.login(
            username='prova5',
            password='test_password_5'
        )
        self.assertTrue(response)

    def test_user_failed_login(self):
        response = self.client.login(
            username='prova5',
            password='wrong_password'
        )
        self.assertFalse(response)

    def test_user_login_redirect(self):
        response = self.client.post(reverse('login'), {
            'username': 'prova5',
            'password': 'test_password_5'
        }, follow=True)
        self.assertRedirects(response, reverse('homepage'))

    def test_users_list_anonymous_user(self):
        response = self.client.get(reverse('users:list'))
        self.assertRedirects(response, '/users/login/?next=/users/')

    def test_users_list_logged_user(self):
        self.client.login(
            username='prova5',
            password='test_password_5'
        )
        response = self.client.get(reverse('users:list'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['object_list']), 2)

    def test_profile_view(self):
        response = self.client.get(
            reverse('users:profile', kwargs={'pk': 1})
        )
        account = response.context['object']
        self.assertEqual(account.username, 'prova1')
        self.assertEqual(account.email, 'prova1@email.com')
        self.assertNotContains(response, "This profile is private!")
        response = self.client.get(
            reverse('users:profile', kwargs={'pk': 2})
        )
        self.assertContains(response, "This profile is private!")

    def test_personal_profile_view(self):
        self.client.login(
            username='prova2',
            password='test_password_2'
        )
        response = self.client.get(
            reverse('users:profile', kwargs={'pk': 3})
        )
        self.assertContains(response, "This profile is private!")
        response = self.client.get(
            reverse('users:profile', kwargs={'pk': 2})
        )
        self.assertNotContains(response, "This profile is private!")

    def test_user_vote_anonymous(self):
        response = self.client.post(reverse('users:vote-user', kwargs={'pk': 1}))
        user_voted = get_object_or_404(UniUser, id=1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(user_voted.votes.count(), 0)

    def test_user_vote_unvote_logged(self):
        self.client.login(
            username='prova2',
            password='test_password_2'
        )
        response = self.client.post(reverse('users:vote-user', kwargs={'pk': 1}))
        user_voted = get_object_or_404(UniUser, id=1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(user_voted.votes.count(), 1)

        response = self.client.post(reverse('users:vote-user', kwargs={'pk': 1}))
        user_voted = get_object_or_404(UniUser, id=1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(user_voted.votes.count(), 0)

    def test_user_is_mod(self):
        self.client.login(
            username='prova2',
            password='test_password_2'
        )
        self.client.post(reverse('users:vote-user', kwargs={'pk': 1}))
        self.client.login(
            username='prova1',
            password='test_password_1'
        )
        self.client.post(reverse('users:vote-user', kwargs={'pk': 1}))
        user_voted = get_object_or_404(UniUser, id=1)
        self.assertEqual(user_voted.votes.count(), 2)
        group = Group.objects.get(name="mods")
        self.assertTrue(group in user_voted.groups.all())

    def test_user_removed_from_mod(self):
        self.client.login(
            username='prova2',
            password='test_password_2'
        )
        self.client.post(reverse('users:vote-user', kwargs={'pk': 1}))
        self.client.login(
            username='prova1',
            password='test_password_1'
        )
        self.client.post(reverse('users:vote-user', kwargs={'pk': 1}))
        self.client.post(reverse('users:vote-user', kwargs={'pk': 1}))
        user_voted = get_object_or_404(UniUser, id=1)
        self.assertEqual(user_voted.votes.count(), 1)
        group = Group.objects.get(name="mods")
        self.assertFalse(group in user_voted.groups.all())

    def test_mod_permission(self):
        self.client.login(
            username='prova2',
            password='test_password_2'
        )
        self.client.post(reverse('users:vote-user', kwargs={'pk': 1}))
        mods, _ = Group.objects.get_or_create(name="mods")
        permissions = mods.permissions.all()
        content_type_topic = ContentType.objects.get(app_label='forum', model='topic')
        content_type_post = ContentType.objects.get(app_label='forum', model='post')
        content_type_comment = ContentType.objects.get(app_label='forum', model='comment')
        content_type_events = ContentType.objects.get(app_label='events', model='event')
        content_type_reviews = ContentType.objects.get(app_label='reviews', model='review')
        # get all permssions for these models
        perms = [Permission.objects.filter(content_type=content_type_topic),
                 Permission.objects.filter(content_type=content_type_post),
                 Permission.objects.filter(content_type=content_type_comment),
                 Permission.objects.filter(content_type=content_type_events),
                 Permission.objects.filter(content_type=content_type_reviews)]
        for e in perms:
            for p in e:
                self.assertTrue(p in permissions)


class UserVoteTestCase(TestCase):

    def setUp(self):
        UniUser.objects.create_user(email="prova1@email.com", username="prova1",
                                    first_name="prova", last_name="va",
                                    password="test_password_1", private=False)
        UniUser.objects.create_user(email="prova2@email.com", username="prova2",
                                    password="test_password_2", private=True)
        Topic.objects.create(name="Tecnologie web", is_exam=True)
        Topic.objects.create(name="Tasse", is_exam=False)

    def test_add_user_mark(self):
        self.client.login(
            username='prova2',
            password='test_password_2'
        )
        response = self.client.post(reverse('users:add-mark'), {
            'exam': 1,
            'vote': 28,
            'honors': False
        })
        user = auth.get_user(self.client)
        self.assertRedirects(response, reverse('users:profile', kwargs={'pk': user.pk}))
        self.assertEqual(UserExamVote.objects.filter(user=user).count(), 1)
        self.assertEqual(UserExamVote.objects.count(), 1)

    def test_add_existing_mark(self):
        self.client.login(
            username='prova2',
            password='test_password_2'
        )
        self.client.post(reverse('users:add-mark'), {
            'exam': 1,
            'vote': 28,
            'honors': False
        })
        with self.assertRaises(ObjectDoesNotExist) as e:
            self.client.post(reverse('users:add-mark'), {
                'exam': 1,
                'vote': 29,
                'honors': False
            })
            self.assertEqual(ObjectDoesNotExist, type(e.exception))
        user = auth.get_user(self.client)
        self.assertEqual(UserExamVote.objects.filter(user=user).count(), 1)
        self.assertEqual(UserExamVote.objects.count(), 1)

    def test_add_anonymous_user_mark(self):
        response = self.client.post(reverse('users:add-mark'), {
            'exam': 1,
            'vote': 28,
            'honors': False
        })
        self.assertEqual(response.status_code, 302)
        self.assertEqual(UserExamVote.objects.count(), 0)

    def test_range_mark(self):
        self.client.login(
            username='prova2',
            password='test_password_2'
        )

        response = self.client.post(reverse('users:add-mark'), {
                'exam': 1,
                'vote': 17,
                'honors': False
            })

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Ensure this value is greater than or equal to 18.")
        response = self.client.post(reverse('users:add-mark'), {
                'exam': 1,
                'vote': 31,
                'honors': False
            })

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Ensure this value is less than or equal to 30.")

    def test_honors(self):
        self.client.login(
            username='prova2',
            password='test_password_2'
        )
        response = self.client.post(reverse('users:add-mark'), {
            'exam': 1,
            'vote': 29,
            'honors': True
        })
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Honors could be assigned only if vote is equal to 30")
        response = self.client.post(reverse('users:add-mark'), {
            'exam': 1,
            'vote': 30,
            'honors': True
        }, follow=True)
        user = auth.get_user(self.client)
        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, reverse("users:profile", kwargs={'pk': user.pk}))
        self.assertContains(response, "Mark added successfully!")

    def test_add_mark_topic_is_not_exam(self):
        self.client.login(
            username='prova2',
            password='test_password_2'
        )
        with self.assertRaises(ObjectDoesNotExist) as e:
            self.client.post(reverse('users:add-mark'), {
                'exam': 2,
                'vote': 29,
                'honors': False
            })
            self.assertEqual(ObjectDoesNotExist, type(e.exception))
        user = auth.get_user(self.client)
        self.assertEqual(UserExamVote.objects.count(), 0)