from django.contrib import admin
from .models import UniUser, UserExamVote


class IsModFilter(admin.SimpleListFilter):
    title = 'moderators'
    parameter_name = 'is_mod'

    def lookups(self, request, model_admin):
        return (
            ('Yes', 'Yes'),
            ('No', 'No'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'Yes':
            return queryset.filter(groups__name__in=['mods', ])
        elif value == 'No':
            return queryset.exclude(groups__name__in=['mods', ])
        return queryset


@admin.register(UniUser)
class UniUserAdmin(admin.ModelAdmin):
    list_display = ("username", "email", "first_name", "last_name", "number_of_votes", "is_mod")
    list_filter = (IsModFilter,)
    search_fields = ("username__icontains",)

    def number_of_votes(self, obj):
        result = obj.votes.count()
        return result

    def is_mod(self, obj):
        if obj.groups.filter(name="mods"):
            return True
        return False

    is_mod.boolean = True


admin.site.register(UserExamVote)
