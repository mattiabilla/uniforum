from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Avg
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, TemplateView, UpdateView, DeleteView

from forum.models import Topic
from forum.mixins import PermOrAuthorMixin
from reviews.forms import ReviewCreationForm, ReviewUpdateForm
from reviews.models import Review
from users.models import UserExamVote


class ReviewListView(ListView):
    model = Review
    template_name = 'reviews/view_reviews.html'
    paginate_by = 2

    def get_queryset(self):
        objects = self.model.objects.all()
        objects = objects.filter(exam=self.kwargs['pk'])

        order = self.request.GET.get("order")
        if order == "least_recent":
            objects = objects.order_by("created_on")
        elif order == "most_recent":
            objects = objects.order_by("-created_on")
        else:
            objects = objects.order_by("-created_on")

        return objects

    def get_context_data(self, **kwargs):
        context = super(ReviewListView, self).get_context_data(**kwargs)
        context['exam'] = get_object_or_404(Topic, pk=self.kwargs['pk'])
        context['order'] = ""
        reviews = self.model.objects.all().filter(exam=self.kwargs['pk'])
        context["avg_subject"] = reviews.aggregate(Avg('subject'))['subject__avg']
        context["avg_lectures"] = reviews.aggregate(Avg('lectures'))['lectures__avg']
        context["avg_test"] = reviews.aggregate(Avg('test'))['test__avg']
        context["vote_distribution"] = []
        votes = UserExamVote.objects.all().filter(exam=self.kwargs['pk'])
        for i in range(18, 31):
            context["vote_distribution"].append(votes.filter(vote=i).count())
        context["vote_average"] = votes.aggregate(Avg('vote'))['vote__avg']
        if self.request.GET.get("order") is not None:
            context['order'] = self.request.GET.get("order")

        if self.request.user.is_authenticated:
            if not reviews.filter(author=self.request.user).exists():
                context["not_reviewed"] = True
        return context


class ReviewCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    form_class = ReviewCreationForm
    template_name = "reviews/add_review.html"
    model = Review
    success_message = "Review added successfully!"

    def get_success_url(self):
        pk = self.kwargs['pk']
        return reverse_lazy('reviews:reviews', kwargs={'pk': pk})

    def get_form(self):
        form = super().get_form()
        form.instance.exam = get_object_or_404(Topic, pk=self.kwargs['pk'])
        return form

    def form_valid(self, form):
        form.instance.author = self.request.user

        return super().form_valid(form)


class ReviewDeleteView(PermOrAuthorMixin, DeleteView):
    perm_required = "reviews.delete_review"
    model = Review
    template_name = 'reviews/delete_review.html'
    success_message = "Review deleted successfully!"

    def get_success_url(self):
        return reverse_lazy('reviews:reviews', kwargs={"pk": self.get_object().exam.pk})

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(ReviewDeleteView, self).delete(request, *args, **kwargs)


class ReviewUpdateView(SuccessMessageMixin, PermOrAuthorMixin, UpdateView):
    perm_required = "reviews.change_review"
    model = Review
    template_name = 'reviews/update_review.html'
    form_class = ReviewUpdateForm
    success_message = "Review updated successfully!"

    def get_success_url(self):
        return reverse_lazy('reviews:reviews', kwargs={"pk": self.get_object().exam.pk})

