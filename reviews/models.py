from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from forum.models import Topic
from users.models import UniUser


class Review(models.Model):
    author = models.ForeignKey(UniUser, blank=False, null=False, on_delete=models.CASCADE)
    exam = models.ForeignKey(Topic, blank=False, null=False, on_delete=models.CASCADE)
    subject = models.IntegerField(
        validators=[MaxValueValidator(5), MinValueValidator(1)],
        blank=False,
        null=False
     )
    lectures = models.IntegerField(
        validators=[MaxValueValidator(5), MinValueValidator(1)],
        blank=False,
        null=False
     )
    test = models.IntegerField(
        validators=[MaxValueValidator(5), MinValueValidator(1)],
        blank=False,
        null=False
     )

    text = models.TextField(blank=True)
    created_on = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["author", "exam"],
                name="A user can have just one review per exam",
            )
        ]

    def clean(self, *args, **kwargs):
        super().clean(*args, **kwargs)
        if not self.exam.is_exam:
            raise ValidationError("You can review an exam")
