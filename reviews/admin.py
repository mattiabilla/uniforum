from django.contrib import admin

from reviews.models import Review


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    list_display = ("exam", "author", "subject", "lectures", "test")
    list_filter = ("exam", "subject", "lectures", "test")
    search_fields = ("text__icontains", )

