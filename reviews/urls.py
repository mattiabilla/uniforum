from django.contrib import admin
from django.urls import path, include

from reviews.views import ReviewListView, ReviewCreateView, ReviewUpdateView, ReviewDeleteView

urlpatterns = [
    path('topic/<int:pk>/', ReviewListView.as_view(), name="reviews"),
    path('topic/<int:pk>/add-review', ReviewCreateView.as_view(), name="add-review"),
    path('<int:pk>/update-review', ReviewUpdateView.as_view(), name="update-review"),
    path('<int:pk>/delete-review', ReviewDeleteView.as_view(), name="delete-review"),
]