from django import forms
from .models import Review


class ReviewCreationForm(forms.ModelForm):

    class Meta(forms.ModelForm):
        model = Review
        fields = ("subject", "lectures", "test", "text")


class ReviewUpdateForm(forms.ModelForm):

    class Meta(forms.ModelForm):
        model = Review
        fields = ("subject", "lectures", "test", "text")