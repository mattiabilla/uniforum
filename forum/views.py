from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from .mixins import PermOrAuthorMixin
from .models import Topic, Post, Comment
from .forms import TopicCreationForm, TopicUpdateForm, PostCreationForm, PostUpdateForm, CommentCreationForm, \
    CommentUpdateForm
from django.db.models import Q, Count


class TopicListView(ListView):
    model = Topic
    template_name = 'forum/topics.html'

    ordering = ['name']


class TopicCreateView(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'forum.add_topic'
    form_class = TopicCreationForm
    model = Topic
    template_name = "forum/add_topic.html"
    success_url = reverse_lazy('forum:topics')
    success_message = "Topic created successfully!"


class TopicUpdateView(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'forum.change_topic'
    model = Topic
    template_name = 'forum/update_topic.html'
    success_url = reverse_lazy('forum:topics')
    form_class = TopicUpdateForm
    success_message = "Topic updated successfully!"


class TopicDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'forum.delete_topic'
    model = Topic
    template_name = 'forum/delete_topic.html'
    success_url = reverse_lazy('forum:topics')
    success_message = "Topic deleted successfully!"

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(TopicDeleteView, self).delete(request, *args, **kwargs)


class TopicView(ListView):
    model = Post
    template_name = 'forum/view_topic.html'
    paginate_by = 2

    def get_queryset(self):
        query = self.request.GET.get("query")
        objects = self.model.objects.all()
        if query:
            objects = objects.filter(Q(topic=self.kwargs['pk']) &
                                     Q(title__icontains=query) | Q(content__icontains=query))
        else:
            objects = objects.filter(topic=self.kwargs['pk'])

        order = self.request.GET.get("order")
        if order == "least_recent":
            objects = objects.order_by("created_on")
        elif order == "most_recent":
            objects = objects.order_by("-created_on")
        elif order == "least_liked":
            objects = objects.annotate(num_likes=Count("likes")).order_by("num_likes")
        elif order == "most_liked":
            objects = objects.annotate(num_likes=Count("likes")).order_by("-num_likes")
        else:
            objects = objects.order_by("-created_on")

        return objects

    def get_context_data(self, **kwargs):
        context = super(TopicView, self).get_context_data(**kwargs)
        context['topic'] = get_object_or_404(Topic, pk=self.kwargs['pk'])
        context['search'] = ""
        context['order'] = ""
        if self.request.GET.get("query") is not None:
            context['search'] = self.request.GET.get("query")
        if self.request.GET.get("order") is not None:
            context['order'] = self.request.GET.get("order")
        return context


class PostCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    form_class = PostCreationForm
    template_name = "forum/add_post.html"
    model = Post
    success_message = "Post created successfully!"

    def get_success_url(self):
        pk = self.kwargs['pk']
        return reverse_lazy('forum:view-topic', kwargs={'pk': pk})

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.instance.topic = get_object_or_404(Topic, pk=self.kwargs['pk'])
        return super().form_valid(form)


class PostDeleteView(PermOrAuthorMixin, DeleteView):
    perm_required = "forum.delete_post"
    model = Post
    template_name = 'forum/delete_post.html'
    success_message = "Post deleted successfully!"

    def get_success_url(self):
        return reverse_lazy('forum:view-topic', kwargs={"pk": self.get_object().topic.pk})

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(PostDeleteView, self).delete(request, *args, **kwargs)


class PostUpdateView(SuccessMessageMixin, PermOrAuthorMixin, UpdateView):
    perm_required = "forum.change_post"
    model = Post
    template_name = 'forum/update_post.html'
    form_class = PostUpdateForm
    success_message = "Post updated successfully!"

    def get_success_url(self):
        return reverse_lazy('forum:view-topic', kwargs={"pk": self.get_object().topic.pk})


class PostView(LoginRequiredMixin, ListView):
    model = Comment
    template_name = 'forum/view_post.html'
    paginate_by = 2

    def get_queryset(self):
        objects = self.model.objects.all()

        objects = objects.filter(post=self.kwargs['pk']).order_by("created_on")

        return objects

    def get_context_data(self, **kwargs):
        context = super(PostView, self).get_context_data(**kwargs)
        context['post'] = get_object_or_404(Post, pk=self.kwargs['pk'])
        context['form'] = CommentCreationForm()
        return context

    def get_success_url(self):
        return reverse_lazy('forum:view-post', kwargs={"pk": self.kwargs['pk']})


class CommentCreateView(LoginRequiredMixin, View):
    success_message = "Comment added successfully!"

    def post(self, request, *args, **kwargs):
        form = CommentCreationForm(request.POST)
        form.instance.author = self.request.user
        form.instance.post = get_object_or_404(Post, id=self.kwargs['pk'])
        if form.is_valid():
            messages.success(self.request, self.success_message)
            form.save()

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class CommentDeleteView(PermOrAuthorMixin, DeleteView):
    perm_required = "forum.delete_comment"
    model = Comment
    template_name = 'forum/delete_comment.html'
    success_message = "Comment deleted successfully!"

    def get_context_data(self, **kwargs):
        context = super(CommentDeleteView, self).get_context_data(**kwargs)
        context['post'] = get_object_or_404(Post, pk=self.get_object().post.pk)
        return context

    def get_success_url(self):
        return reverse_lazy('forum:view-post', kwargs={"pk": self.get_object().post.pk})

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(CommentDeleteView, self).delete(request, *args, **kwargs)


class CommentUpdateView(SuccessMessageMixin, PermOrAuthorMixin, UpdateView):
    perm_required = "forum.change_comment"
    model = Comment
    template_name = 'forum/update_comment.html'
    form_class = CommentUpdateForm
    success_message = "Comment updated successfully!"

    def get_context_data(self, **kwargs):
        context = super(CommentUpdateView, self).get_context_data(**kwargs)
        context['post'] = get_object_or_404(Post, pk=self.get_object().post.pk)
        return context

    def get_success_url(self):
        return reverse_lazy('forum:view-post', kwargs={"pk": self.get_object().post.pk})


class PostLikeView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        post_liked = get_object_or_404(Post, id=self.kwargs['pk'])
        if post_liked.likes.filter(id=request.user.id).exists():
            post_liked.likes.remove(request.user)
        else:
            post_liked.likes.add(request.user)
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class CommentLikeView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        comment_liked = get_object_or_404(Comment, id=self.kwargs['pk'])
        if comment_liked.likes.filter(id=request.user.id).exists():
            comment_liked.likes.remove(request.user)
        else:
            comment_liked.likes.add(request.user)
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
