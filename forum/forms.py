from django import forms
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _

from .models import Post, Topic, Comment


class TopicCreationForm(forms.ModelForm):

    class Meta(forms.ModelForm):
        model = Topic
        fields = ("name", "is_exam")
        success_url = reverse_lazy('forum:topics')
        labels = {
            'is_exam': _('Is the topic an exam?'),
        }


class TopicUpdateForm(forms.ModelForm):

    class Meta(forms.ModelForm):
        model = Topic
        fields = ("name", "is_exam")
        success_url = reverse_lazy('forum:topics')
        labels = {
            'is_exam': _('Is the topic an exam?'),
        }


class PostCreationForm(forms.ModelForm):

    class Meta(forms.ModelForm):
        model = Post
        fields = ("title", "content")


class PostUpdateForm(forms.ModelForm):

    class Meta(forms.ModelForm):
        model = Post
        fields = ("title", "content")

class CommentCreationForm(forms.ModelForm):

    class Meta(forms.ModelForm):
        model = Comment
        fields = ("content",)

class CommentUpdateForm(forms.ModelForm):

    class Meta(forms.ModelForm):
        model = Comment
        fields = ("content",)