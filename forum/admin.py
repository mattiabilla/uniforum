from django.contrib import admin

from .models import Topic, Post, Comment


@admin.register(Topic)
class TopicAdmin(admin.ModelAdmin):
    list_display = ("name", "is_exam", "posts")
    list_filter = ('is_exam',)
    search_fields = ("name__icontains",)

    def posts(self, obj):
        result = Post.objects.filter(topic=obj).count()
        return result


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ("title", "topic", "author", "created_on", "comments")
    list_filter = ("topic",)
    search_fields = ("title__icontains", "content__icontains")

    def comments(self, obj):
        result = Comment.objects.filter(post=obj).count()
        return result


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ("post", "author", "created_on")
    search_fields = ("post__title__icontains", "content__icontains")
