from django.urls import path
from .views import CommentCreateView, TopicListView, TopicCreateView, TopicUpdateView, TopicDeleteView, TopicView, \
    PostCreateView, \
    PostUpdateView, PostDeleteView, PostView, CommentDeleteView, CommentUpdateView, PostLikeView, CommentLikeView

app_name = 'forum'

urlpatterns = [
    path('', TopicListView.as_view(), name="topics"),
    path('add-topic', TopicCreateView.as_view(), name="add-topic"),
    path('topic/<int:pk>/update/', TopicUpdateView.as_view(), name="update-topic"),
    path('topic/<int:pk>/delete/', TopicDeleteView.as_view(), name="delete-topic"),
    path('post/<int:pk>/update/', PostUpdateView.as_view(), name="update-post"),
    path('post/<int:pk>/delete/', PostDeleteView.as_view(), name="delete-post"),
    path('post/<int:pk>/', PostView.as_view(), name="view-post"),
    path('topic/<int:pk>/', TopicView.as_view(), name="view-topic"),
    path('topic/<int:pk>/add-post', PostCreateView.as_view(), name="add-post"),
    path('post/<int:pk>/add-comment', CommentCreateView.as_view(), name="add-comment"),
    path('comment/<int:pk>/delete-comment', CommentDeleteView.as_view(), name="delete-comment"),
    path('comment/<int:pk>/update-comment', CommentUpdateView.as_view(), name="update-comment"),
    path('post/<int:pk>/like-post', PostLikeView.as_view(), name="like-post"),
    path('comment/<int:pk>/like-comment', CommentLikeView.as_view(), name="like-comment"),
]
