from django.contrib.auth.mixins import UserPassesTestMixin


class PermOrAuthorMixin(UserPassesTestMixin):
    def test_func(self):
        return (self.request.user == self.get_object().author) or (self.request.user.has_perm(self.perm_required))