from django.db import models
from django.utils.translation import gettext_lazy as _
from tinymce.models import HTMLField
from users.models import UniUser
from django.utils.timezone import now


class Topic(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False, unique=True)
    is_exam = models.BooleanField(blank=False, null=False)

    def __str__(self):
        return self.name


class Post(models.Model):
    title = models.CharField(max_length=255, blank=False, null=False)
    content = HTMLField()
    author = models.ForeignKey(UniUser, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True, editable=False)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    likes = models.ManyToManyField(UniUser, symmetrical=False, related_name="likes", blank=True)

    def __str__(self):
        return self.title


class Comment(models.Model):
    content = HTMLField()
    author = models.ForeignKey(UniUser, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True, editable=False)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    likes = models.ManyToManyField(UniUser, symmetrical=False, related_name="comment_likes", blank=True)

    def __str__(self):
        return self.post.title+" - "+self.author.username
