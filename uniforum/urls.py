from django.contrib import admin
from django.urls import path, include

from django.conf import settings
from django.conf.urls.static import static

from .views import Homepage

urlpatterns = [
    path('', Homepage.as_view(), name='homepage'),
    path('admin/', admin.site.urls),
    path('users/', include(('users.urls', 'users'))),
    path('users/', include('django.contrib.auth.urls')),
    path('forum/', include(('forum.urls', 'forum'))),
    path('events/', include(('events.urls', 'events'))),
    path('reviews/', include(('reviews.urls', 'reviews'))),
    path('tinymce/', include('tinymce.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)