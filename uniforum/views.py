from django.views.generic import ListView

from events.models import Event


class Homepage(ListView):
    template_name = 'home.html'
    model = Event
