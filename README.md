# Uniforum

### Descrizione

Uniforum è una piattaforma pensata per gli studenti di un corso universitario su cui è possibile condividere informazioni riguardanti i diversi insegnamenti e la vita universitaria.
Ogni studente può creare un profilo personale, porre domande e interagire con gli altri studenti.
Ciò che più differenzia Uniforum è la possibilità data agli iscritti di eleggere i moderatori in ogni momento.

### Utenti

Le tipologie di utenti che possono accedere ed utilizzare Uniforum sono tre (oltre all’amministratore) con differenti permessi e privilegi:
* Anonimo: può visualizzare gli argomenti, i relativi eventi e, se l’argomento è un corso, visualizzarne le statistiche. Inoltre può anche visualizzare la bacheca di un utente (a seconda del livello di privacy impostato)
* Iscritto: può pubblicare, modificare e visualizzare post su un argomento, ha un profilo personale in cui inserire le proprie informazioni, impostando un livello di privacy, e può lasciare un feedback ai post ed eleggere i moderatori. 
* Moderatore: un utente iscritto può essere eletto moderatore e avere così maggiori privilegi. Un moderatore può creare o eliminare gli argomenti e i relativi post (moderando così le pubblicazioni). Può inoltre aggiungere, modificare e visualizzare eventi legati ad un argomento (ad esempio le date di esami o la scadenza delle tasse).

### Funzionalità

I post di Uniforum sono divisi per argomenti (creati, modificati ed eliminati dai moderatori), gli argomenti possono essere generici o relativi ad un insegnamento. Ogni argomento può avere una lista di eventi aggiornati dai moderatori e visualizzabili da tutti gli utenti sotto forma di calendario. Se l’argomento è un insegnamento è possibile, per ogni utente iscritto, aggiungere il proprio voto così da mostrarne la distribuzione (mediante un grafico) e quindi la difficoltà. É possibile inoltre aggiungere una recensione.
Una recensione è divisa in 3 categorie:
* Subject
* Lessons
* Test

Inoltre è possibile aggiungere una recensione scritta.

Un utente iscritto può pubblicare un post relativo ad un argomento. Gli utenti possono lasciare un feedback sotto forma di “mi piace” oppure con un commento in risposta. Gli utenti possono modificare o eliminare i propri post o commenti e, se moderatori, anche quelli degli altri utenti (moderando così i contenuti). All’interno di un argomento è possibile cercare i post e visualizzarli in ordine di creazione oppure di “mi piace”.
Ogni utente iscritto ad Uniforum ha un profilo personale visibile a tutti, nel quale può aggiungere un’immagine e altre informazioni, come ad esempio i voti degli esami che ha superato. Il profilo può essere pubblico o privato, nel secondo caso i voti non verranno mostrati agli altri utenti.
Una particolarità di Uniforum è che gli utenti iscritti possono esprimere la loro preferenza per eleggere i moderatori della piattaforma. Gli utenti che ottengono un numero sufficiente di voti diventano moderatori, inoltre ogni utente può modificare in qualsiasi momento la propria scelta.

### Installazione

Per installare l'applicazione seguire questi passi:
* Clonare la repository
* Spostarsi nella cartella "uniforum"
* Lanciare il comando `pipenv install` dal terminale
* Entrare nel virtualenv con `pipenv shell`
* Lanciare il comando `python manage.py runserver`

Il progetto contiene un database con utenti di prova, un superuser (admin) e dei topic con post, commenti, recensioni ed eventi.
La password per l'utente admin è "admin", mentre per tutti gli altri utenti di prova è "p12345678".
Il server di manage.py non è adatto alla distribuzione.
